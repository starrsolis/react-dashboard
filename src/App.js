//import FAQ from "./Scenes/faq";
//import Calendar from "./Scenes/calendar";
//import Bar from "./Scenes/bar";
//import Pie from "./Scenes/pie";
import { CssBaseline, ThemeProvider } from "@mui/material";
import Sidebar from "./Scenes/global/Sidebar";
import Topbar from "./Scenes/global/Topbar";
import { ColorModeContext, useMode } from "./theme";
import Dashboard from "./Scenes/dashboard";
import { Route, Routes} from "react-router";
import Team from "./Scenes/team"
import Contacts from "./Scenes/contacts"
import Invoices from "./Scenes/invoices";
import Form from "./Scenes/form";


function App() {
  const [theme, colorMode] = useMode();
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          <Sidebar />
          <main className="content">
            <Topbar />
            <Routes>
              <Route path="/" element={<Dashboard />} />
              <Route path="/team" element={<Team />} />
              <Route path="/contacts" element={<Contacts />} />
              <Route path="/invoices" element={<Invoices />} />
              <Route path="/form" element={<Form />} />
            </Routes>
          </main>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;